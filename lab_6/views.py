from django.shortcuts import render
from django.http import HttpResponseRedirect

response = {}
def index(request):
    response['author'] = "Shavira Adeva"
    return render(request, 'lab_6/lab_6.html', response)

